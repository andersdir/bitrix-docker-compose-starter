# Bitrix docker compose starter

## Build containers
1. Copy example.env to .env and change some settings if you need.
2. Run ```docker-compose up -d```.
3. Done. If you are use .env with default settings:
- Web site will run on localhost
- Adminer on localhost:8080 ( root / root )
- DB connection for adminer:
    - Host: db
    - Login: root
    - Password: root
    - Database: database

## Install Bitrix
1. Install Bitrix in app folder.
DB connection for bitrix: 
- Host: db:3306
- Login: root
- Password: root
- Database: database